import React, {Component, Fragment} from 'react';
import {Route, Switch} from "react-router-dom";

import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Layout from "./containers/Layout/Layout";
import Posts from "./containers/Posts/Posts";
import PostItem from "./containers/PostItem/PostItem";

class App extends Component {
  render() {
    return (
        <Fragment>
            <Layout>
                <Switch>
                    <Route path="/" exact component={Posts}/>
                    <Route path="/posts/:id" exact component={PostItem}/>
                    <Route path="/register" exact component={Register}/>
                    <Route path="/login" exact component={Login}/>
                </Switch>
            </Layout>
        </Fragment>
    );
  }
}


export default App;

