import React, {Component, Fragment} from 'react';
import FormElement from "../UI/Form/FormElement";
import {Button, Col, Form, FormGroup} from "reactstrap";
import {connect} from "react-redux";
import {addComment} from "../../store/actions/commentsActions";

class AddComment extends Component {

    state = {
        comment: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = {post: this.props.id, title: this.state.comment};
        this.props.addComment(formData, this.props.id);
    };

    render() {
        return (
            <Fragment>
                <Form onSubmit={this.submitFormHandler}>
                    <FormElement
                        propertyName="comment"
                        type="text"
                        title="Comment"
                        value={this.state.comment}
                        onChange={this.inputChangeHandler}
                        placeholder="Enter comment"
                    />
                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" color="primary">
                                Add
                            </Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addComment: (data, id) => dispatch(addComment(data, id))
    }
};

export default connect(null, mapDispatchToProps)(AddComment);