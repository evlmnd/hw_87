import React, {Fragment} from 'react';
import {
    Nav,
    Navbar, NavbarBrand,
    NavItem,
    NavLink
} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';
import AnonymousMenu from "./Menus/AnonymousMenu";
import UserMenu from "./Menus/UserMenu";

const Toolbar = ({user, logout}) => {


    return (

        <Fragment>
            <Navbar color="light" light expand="md">
                <NavbarBrand href="/">SHOP</NavbarBrand>
                <Nav className="ml-auto" navbar>
                    {user ?
                        <NavItem>
                            <NavLink tag={RouterNavLink} to="/create_post" exact>Create post</NavLink>
                        </NavItem>
                        : null}
                    {user ? <UserMenu user={user} logout={logout}/> : <AnonymousMenu/>}
                </Nav>
            </Navbar>
        </Fragment>

    );
};

export default Toolbar;
