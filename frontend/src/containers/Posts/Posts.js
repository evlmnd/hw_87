import React, {Component, Fragment} from "react";
import { connect } from "react-redux";
import {CardDeck, Col} from "reactstrap";
import CardItem from "../../components/CardItem/CardItem";
import {getPosts} from "../../store/actions/postsActions";

class Posts extends Component {

    getPostItem = id => {
        this.props.history.push('/posts/' + id);
    };

    componentDidMount() {
        this.props.getPosts();
    }

    render() {
        return (
            <Fragment>
                <CardDeck>
                    {this.props.posts.map(item => (
                        <Col sm={3} key={item._id}>
                            <CardItem
                                id={item._id}
                                imageSrc={item.image}
                                subtitle={item.datetime.slice(0, 10) + ', ' + item.datetime.slice(11, 16)}
                                text={'By ' + item.user.username}
                                title={item.title}
                                click={() => this.getPostItem(item._id)}
                            />
                        </Col>
                    ))}
                </CardDeck>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        posts: state.posts.posts,
        user: state.users.user
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getPosts: () => dispatch(getPosts())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Posts);
