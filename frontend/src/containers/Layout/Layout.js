import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Container} from "reactstrap";

import {logoutUser} from "../../store/actions/usersActions";
import Toolbar from "../../components/UI/Toolbar/Toolbar";

class Layout extends Component {


    render() {

        return (
            <Fragment>
                <header>
                    <Toolbar
                        user={this.props.user}
                        logout={this.props.logoutUser}
                    />
                </header>
                <Container style={{marginTop: '20px'}}>
                    {this.props.children}
                </Container>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default connect(mapStateToProps, mapDispatchToProps)(Layout);