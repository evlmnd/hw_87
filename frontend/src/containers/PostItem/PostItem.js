import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import {Jumbotron, ListGroup, ListGroupItem} from "reactstrap";
import {getPostItem} from "../../store/actions/postsActions";
import {getComments} from "../../store/actions/commentsActions";
import AddComment from "../../components/AddComment/AddComment";


class FullPost extends Component {
    componentDidMount() {
        this.props.getPostItem(this.props.match.params.id);
        this.props.getComments(this.props.match.params.id);
    }

    render() {
        return (
            <Fragment>
                {this.props.postItem.datetime ? (
                    <Jumbotron>
                        <h1>
                            {this.props.postItem.title}
                        </h1>
                        <div><i>{this.props.postItem.datetime.slice(0, 10) + ', ' + this.props.postItem.datetime.slice(11, 16)}</i></div>
                        <p> {this.props.postItem.description}</p>

                        <ListGroup>
                            {this.props.comments.map(item => {
                                return <ListGroupItem key={item._id}>
                                    [{item.user.username}]: {item.title}
                                </ListGroupItem>
                            })}
                        </ListGroup>

                            {this.props.user && (
                                <AddComment
                                    id={this.props.match.params.id}
                                />
                            )}
                    </Jumbotron>
                ) : null}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        postItem: state.posts.postItem,
        user: state.users.user,
        comments: state.comments.comments
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getPostItem: id => dispatch(getPostItem(id)),
        getComments: id => dispatch(getComments(id))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FullPost);
