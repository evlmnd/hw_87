import axios from "../../axios-api";

export const GET_POSTS_SUCCESS = 'GET_POSTS_SUCCESS';
export const GET_POSTS_FAILURE = 'GET_POSTS_FAILURE';

export const GET_POST_ITEM_SUCCESS = 'GET_POST_ITEM_SUCCESS';
export const GET_POST_ITEM_FAILURE = 'GET_POST_ITEM_FAILURE';


const getPostsSuccess = posts => ({type: GET_POSTS_SUCCESS, posts});
const getPostsFailure = error => ({type: GET_POSTS_FAILURE, error});

export const getPosts = () => {
    return dispatch => {
        return axios.get('/posts').then(
            response => {
                dispatch(getPostsSuccess(response.data));
            },
            error => {
                dispatch(getPostsFailure(error));
            }
        );
    };
};

const getPostItemSuccess = post => ({type: GET_POST_ITEM_SUCCESS, post});
const getPostItemFailure = post => ({type: GET_POST_ITEM_FAILURE, post});

export const getPostItem = id => {
    return dispatch => {
        return axios.get(`/posts/${id}`).then(
            response => {
                dispatch(getPostItemSuccess(response.data));
            },
            error => {
                dispatch(getPostItemFailure(error));
            }
        );
    };
};