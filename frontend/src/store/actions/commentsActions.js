import axios from '../../axios-api';

export const GET_COMMENTS_SUCCESS = 'GET_COMMENTS_SUCCESS';
export const GET_COMMENTS_FAILURE = 'GET_COMMENTS_FAILURE';

export const ADD_COMMENT_SUCCESS = 'ADD_COMMENT_SUCCESS';
export const ADD_COMMENT_FAILURE = 'ADD_COMMENT_FAILURE';

const getCommentsSuccess = data => ({type: GET_COMMENTS_SUCCESS, data});
const getCommentsFailure = error => ({type: GET_COMMENTS_FAILURE, error});

const addCommentSuccess = data => ({type: ADD_COMMENT_SUCCESS, data});
const addCommentFailure = data => ({type: ADD_COMMENT_FAILURE, data});

export const getComments = id => {
    return dispatch => {
        return axios.get(`/comments/${id}`).then(
            response => {
                console.log(response.data);
                dispatch(getCommentsSuccess(response.data));
            }, error => {
                dispatch(getCommentsFailure(error));
            }
        );
    };
};

export const addComment = (data, id) => {
    return (dispatch, getState) => {
        const token = getState().users.user.user.token;
        const headers = {'Token': token};
        return axios.post('/comments', data, {headers}).then(
            response => {
                dispatch(addCommentSuccess(response.data));
                dispatch(getComments(id))
            },
            error => {
                dispatch(addCommentFailure(error));
            }
        );
    };
};