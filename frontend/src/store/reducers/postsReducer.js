import {
    GET_POST_ITEM_FAILURE,
    GET_POST_ITEM_SUCCESS,
    GET_POSTS_FAILURE,
    GET_POSTS_SUCCESS
} from "../actions/postsActions";

const initialState = {
    posts: [],
    error: null,
    postItem: []
};

const postsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_POSTS_SUCCESS:
            return {...state, posts: action.posts};
        case GET_POSTS_FAILURE:
            return {...state, error: action.error};
        case GET_POST_ITEM_SUCCESS:
            return {...state, postItem: action.post};
        case GET_POST_ITEM_FAILURE:
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default postsReducer;