import {
    ADD_COMMENT_FAILURE,
    ADD_COMMENT_SUCCESS,
    GET_COMMENTS_FAILURE,
    GET_COMMENTS_SUCCESS
} from "../actions/commentsActions";

const initialState = {
    comments: [],
    error: null
};

const commentsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_COMMENTS_SUCCESS:
            return {...state, comments: action.data};
        case GET_COMMENTS_FAILURE:
            return {...state, error: action.error};
        case ADD_COMMENT_SUCCESS:
            return {...state, error: null};
        case ADD_COMMENT_FAILURE:
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default commentsReducer;